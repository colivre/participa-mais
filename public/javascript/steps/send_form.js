(function() {

  var serialNumber = 1

  $(document).ready(function() {

    $('form#new_step_form').submit(function() {
      event.preventDefault()
      sendForm()
    })
  })

  function sendForm() {
    var form = $('form#new_step_form').get(0)
    var formData = new FormData(form)

    $.each(UploadedFiles.files, function(key, list_files) {
       list_files.map.forEach(function(file) {
          formData.append('denouncement_files[]', file)
       })
    })

    $.ajax({
      url: form.getAttribute('action'),
      data: formData,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function(data){
        if(data.location) {
          window.location.href = data.location
        } else {
          $("#denouncement-content").html(data)
        }
      }
    })
  }
})()
