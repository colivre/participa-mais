function setPushToken(token, uuid) {
  $.post('/plugin/participa_mais/push_token/set', {
    token: token,
    uuid: uuid
  });
}
