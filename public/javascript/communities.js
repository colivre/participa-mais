$(document).ready(function() {
  let content          = $("#content");
  let communitiesBlock = $("#theme-header .participa-communities-block");
  let profileBlock     = $("#theme-footer .profile-description-block");
  content.prepend(communitiesBlock);
  content.append(profileBlock);

  $('body.controller-search.action-search-communities .main-content h1').html("Todas comunidades");
})
