class RenameFilesToMediaFiles < ActiveRecord::Migration

  def self.up
    rename_table :participa_mais_plugin_files, :participa_mais_plugin_media_files
  end

  def self.down
    rename_table :participa_mais_plugin_media_files, :participa_mais_plugin_files
  end
end
