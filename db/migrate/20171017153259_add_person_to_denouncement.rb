class AddPersonToDenouncement < ActiveRecord::Migration
  def change
    add_reference :participa_mais_plugin_denouncements, :person, index: true
  end
end
