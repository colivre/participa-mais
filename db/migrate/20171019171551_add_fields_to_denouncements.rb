class AddFieldsToDenouncements < ActiveRecord::Migration
  def change
    add_column :participa_mais_plugin_denouncements, :city, :string
    add_column :participa_mais_plugin_denouncements, :address, :string
    add_column :participa_mais_plugin_denouncements, :description, :text
    add_column :participa_mais_plugin_denouncements, :denouncer_id, :integer


    add_column :participa_mais_plugin_denouncements, :created_at, :datetime,
               null: false
    add_column :participa_mais_plugin_denouncements, :updated_at, :datetime,
               null: false

    add_column :participa_mais_plugin_denouncements, :metadata, :jsonb,
               default: {}
    add_index  :participa_mais_plugin_denouncements, :metadata, using: :gin
  end
end
