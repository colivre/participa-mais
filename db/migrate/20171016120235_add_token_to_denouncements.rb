class AddTokenToDenouncements < ActiveRecord::Migration
  def up
    add_column :participa_mais_plugin_denouncements, :token, :string
  end

  def down
    remove_column :participa_mais_plugin_denouncements, :token, :string
  end
end
