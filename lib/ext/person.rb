require_dependency 'person'

class Person

  include ParticipaMaisPlugin::HasDevice

  has_many :denouncements, class_name: 'ParticipaMaisPlugin::Denouncement'

end
