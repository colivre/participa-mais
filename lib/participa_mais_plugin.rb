class ParticipamMaisPlugin < Noosfero::Plugin

  def organization_members(organization)
    if ParticipaMaisPlugin.config['communities'].include?\
        (organization.identifier)
      return Person.all
    end
  end

  class << self
    def plugin_name
      'Participa Mais'
    end

    def plugin_description
      'Um plugin específico para as necessidades do Participa Mais.'
    end

    def config
      begin
        conf = YAML::load(
          File.open(Rails.root + 'plugins/participa-mais/config.yml')
        )
      rescue StandardError => err
        p 'Could not open config.yml file'
        p err
      end
      conf
    end

    def service_address
      ip = ParticipaMaisPlugin.config['dncmt_service']['addr']
      method = ParticipaMaisPlugin.config['dncmt_service']['method']
      port = ParticipaMaisPlugin.config['dncmt_service']['port']
      action = ParticipaMaisPlugin.config['dncmt_service']['endpoint']
      [method, ip, ':', port, '/', action].join
    end

    def extra_blocks
      { ProfileDescriptionBlock => {}, CommunitiesListBlock => {}, HappensBlock => {} }
    end

  end

  def stylesheet?
    true
  end

  def js_files
    #TODO: should we include steps Js locally w/ javascript_iclude_tag?
    [
      'map',
      'steps_bar',
      'toggleable_field',
      'toggle',
      'tokens',
      'fields',
      'tracking',
      'steps/confirmation',
      'steps/identification',
      'steps/occurrence',
      'steps/offender',
      'steps/media_files',
      'steps/send_form',
      'steps/uploaded_files',
      'steps/victims',
      'blocks/blocks',
      'communities'
    ].map { |filename| "javascript/#{filename}" }
  end

  def account_controller_filters
    block = proc do
      extend ParticipaMaisPlugin::CookiesHelper
      if (current_person &&
          cookies[push_token_cookie_key].present? &&
          cookies[device_id_cookie_key].present?)
        current_person.update_devices(cookies[device_id_cookie_key],
                                      cookies[push_token_cookie_key])
      end
    end

    {
      type: 'after_filter',
      method_name: 'participa_mais_plugin_updates_profile_push_token',
      options: { only: 'login' },
      block: block
    }
  end

  def cms_extra_options
    Proc.new { render partial: 'participa_mais_plugin_cms/extra_options' }
  end

  def person_friends(person)
    person.environment.people
  end

  def custom_notification(verb, *args)
    self.send("#{verb}_notification", *args)
  end

  private

  def new_content_notification(content)
    ParticipaMaisPlugin::PushNotifier.deliver(content.tokens, {
      title: content.name,
      message: content.push_notification_message,
      data: {
        type: 'NEW_CONTENT',
        path: content.full_path
      }
    })
  end
end
