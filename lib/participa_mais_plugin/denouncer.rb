class ParticipaMaisPlugin::Denouncer < ApplicationRecord

  include ParticipaMaisPlugin::HasDevice

  attr_accessible  :name, :email, :profile_id
  validates_presence_of :name, :email

  has_many :denouncements, class_name: 'ParticipaMaisPlugin::Denouncement'
  belongs_to :profile

end
