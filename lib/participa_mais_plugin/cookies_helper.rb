module ParticipaMaisPlugin::CookiesHelper

  def push_token_cookie_key
    :_noosfero_plugin_participa_mais_push_token
  end

  def device_id_cookie_key
    :_noosfero_plugin_participa_mais_device_id
  end

  def set_plugin_cookie(key, value)
    cookies[key] = { value: value, path: '/' }
  end

end
